.. ccmsDoc documentation master file, created by
   sphinx-quickstart on Tue Aug 14 12:45:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _index:

====================
Sphinx documentation
====================

.. sectionauthor:: Rijju Maharjan <rijju.maharjan@f1soft.com>

.. toctree::
   :maxdepth: 2

   sphinxTut/index
   sphinxTut/getting_started
   sphinxTut/render_pdf
   restructured/index

==================
Indices and tables
==================

* index_
* :ref:`search`

===========
References:
===========

* `Read The Docs Documentation <https://docs.readthedocs.io>`_
* `Sphinx Documentation <http://www.sphinx-doc.org>`_
