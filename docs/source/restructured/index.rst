
reStructuredText
================

Inline Markup
-------------
.. sidebar:: Result

   * *text*
   * **text**
   * ``text``
   * thisis\ *one*\ word
   * `sphinx <http://www.sphinx-doc.org>`_

::

    *text*   : italics
    **text** : boldface
    ``text`` : code samples/blocks, verbatim mode - used as the escaping character
    |        : vertical bar '|' for line break
    thisis\ *one*\ word : use of backslash to intreprete a single word in different formats
    `sphinx <http://www.sphinx-doc.org>`_ : create links to internal or external web pages

|

Headings
--------
::

    # with overline, for parts
    * with overline, for chapters
    =, for sections
    -, for subsections
    ^, for subsubsections
    “, for paragraphs

Colored Boxes
-------------
[ note, seealso, todo and warnings]

::

    .. note::  This is a **note** box.

.. note::  This is a **note** box.

::

    .. seealso:: This is a simple **seealso** note.

.. seealso:: This is a simple **seealso** note.

::

    .. warning:: note the space between the directive and the text

.. warning:: note the space between the directive and the text

Lists and Quote-like blocks
---------------------------
.. sidebar:: Result

    * List 1
    * List 2

    1. List 1
    2. List 2

    #. List 1
    #. List 2

    * this is
    * a list

      * with a nested list
      * and some subitems

    * and here the parent list continues

Bullet List(* ), Numbered List(numbers or #. )
::

   * List 1
   * List 2

   1. List 1
   2. List 2

   #. List 1
   #. List 2

Nested lists: separated from the parent list items by blank lines
::

   * this is
   * a list

       * with a nested list
       * and some subitems

   * and here the parent list continues

.. sidebar:: Result

   term 1
    Definition 1.

   term 2
       Definition 2, paragraph 1.

       Definition 2, paragraph 2.

   term 3 : classifier
       Definition 3.

   term 4 : classifier one : classifier two
       Definition 4

Term
----
* one-line word or phrase,
* multiple paragraphs,
* no blank line between a term line and a definition block

::

   term 1
    Definition 1.

   term 2
       Definition 2, paragraph 1.

       Definition 2, paragraph 2.

   term 3 : classifier
       Definition 3.

   term 4 : classifier one : classifier two
       Definition 4

|

Directives
----------
A reST directive is like a function – a powerful construct of the reST syntax used to include formatted text,
and it accepts:

* arguments
* options
* body

.. sidebar:: Code Block Example

   .. code-block:: html
      :linenos:

      <h1>Code Block</h1>

::

   .. code-block:: html
      :linenos:

      <h1>Code Block</h1>

code-block directive: html *(argument)*, linenos *(option)*
toctree directive: maxdepth *(option - indicating maximum number of levels in a single menu)*

Code and Literal Blocks
-----------------------
ending a paragraph with the special marker **::**

* preceded by whitespace -> marker is removed
* preceded by non-whitespace -> marker is replaced by a single colon
* if continued with a same paragraph without a blank line, it is completely left out of the document


.. include:: table.rst
.. include:: hyperlinks.rst
.. include:: others.rst

Read more: `reStructuredText <http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_

