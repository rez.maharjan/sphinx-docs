
Table Example
-------------
* grid, simple, csv, list-table syntax
* tabularcolumns directive (column spec - to determine column width (default is left-aligned)

.. sidebar:: Example 1

    .. _making-a-table:

    ==================   ============
    Name                 Age
    ==================   ============
    John D Hunter        40
    Cast of Thousands    41
    And Still More       42
    ==================   ============

**Example 1**::

    ==================   ============
    Name                 Age
    ==================   ============
    John D Hunter        40
    Cast of Thousands    41
    And Still More       42
    ==================   ============

|
|

**Example 2**::

    +------------+------------+-----------+
    | Header 1   | Header 2   | Header 3  |
    +============+============+===========+
    | body row 1 | column 2   | column 3  |
    +------------+------------+-----------+
    | body row 2 | Cells may span columns.|
    +------------+------------+-----------+
    | body row 3 | Cells may  | - Cells   |
    +------------+ span rows. | - contain |
    | body row 4 |            | - blocks. |
    +------------+------------+-----------+

+------------+------------+-----------+
| Header 1   | Header 2   | Header 3  |
+============+============+===========+
| body row 1 | column 2   | column 3  |
+------------+------------+-----------+
| body row 2 | Cells may span columns.|
+------------+------------+-----------+
| body row 3 | Cells may  | - Cells   |
+------------+ span rows. | - contain |
| body row 4 |            | - blocks. |
+------------+------------+-----------+

.. sidebar:: Example 3

    .. tabularcolumns:: |l|c|p{5cm}|

    +--------------+---+-----------+
    |  simple text | 2 | 3         |
    +--------------+---+-----------+

**Example 3**::

    .. tabularcolumns:: |l|c|p{5cm}|

    +--------------+---+-----------+
    |simple text   | 2 | 3         |
    +--------------+---+-----------+

**Example 4**
::

       .. |insta| image:: ../path/to/image
              :width: 50pt
              :height: 30pt

       .. |twit| image:: ../path/to/image
              :width: 30pt
              :height: 30pt

       +---------------------+----------------+
       |  |insta|            |   |twit|       |
       +---------------------+----------------+

.. |insta| image:: ../images/image4.jpg
       :width: 50pt
       :height: 30pt

.. |twit| image:: ../images/image5.jpg
       :width: 30pt
       :height: 30pt

+---------------------+----------------+
|  |insta|            |   |twit|       |
+---------------------+----------------+

.. sidebar:: CSV Example

    .. csv-table:: csv-example
       :header: "Fullname", "Address"
       :widths: 20, 20

       "User A", "Lalitpur,"
       "User B", "Kathmandu"

**CSV Example**::

    .. csv-table:: csv-example
       :header: "Fullname", "Address"
       :widths: 20, 20

       "User A", "Lalitpur,"
       "User B", "Kathmandu"

