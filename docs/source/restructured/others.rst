.. topic:: Example of substitution

   |subsequent|
   the body of the topic, and are
   interpreted as body elements.

Some text that requires a footnote [#f1]_ .

.. hlist::
   :columns: 3

   * first item
   * second item
   * 3d item
   * 4th item
   * 5th item
   * 6th item

:Whatever: this is handy to create new field

:nocomments: some contents

.. rubric:: Footnotes

.. [#f1] Text of the first footnote.


.. |subsequent| replace:: Subsequent indented lines comprise
.. |textReplace| replace:: This is a sample text that has been replaced